import gherkin.ast.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.openqa.jetty.html.Break;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import sun.awt.SunHints;

import javax.swing.*;
import java.io.File;
import java.security.Key;
import java.util.List;
import java.util.concurrent.TimeUnit;


public class stepDefinitions {

   public static WebDriver driver;

    @Given("I launch chrome browser")
    public void i_launch_chrome_browser() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "E:\\chromedriver.exe");
                driver = new ChromeDriver();
                driver.manage().window().maximize();
                driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);


        System.out.println("Browser is open");


    }

    @When("I open NobilisAi homepage")
    public void i_open_NobilisAi_homepage()  {
       try {
           driver.get("http://sandbox.nobilis.ai/");
       }catch (TimeoutException e){
           System.out.println("Browser did not load");
       }


        System.out.println("Browser supports the Nobilis");

    }

    @Then("I verify that the nobilistext present on the page")
    public void i_verify_that_the_nobilistext_present_on_the_page() throws InterruptedException {
        boolean status = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div[1]/div/div/div[1]")).isDisplayed();
        Assert.assertEquals(true,status);

        System.out.println("Logo is present, hence Nobilis site is currenly running");
        Thread.sleep(5000);
    }

    @Then("I enter incorrect username and password")
    public void i_enter_incorrect_username_and_password() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver,30);

        driver.findElement(By.xpath("//*[@id=\"normal_login_username\"]")).sendKeys("santosh");
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
        Thread.sleep(4000);

        if (driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div[2]/div[1]/div/div/form/div[3]/div/div/span")).isDisplayed()){
            System.out.println("Correct message for incorrect email is shown");
        } else {
            System.out.println("Message not shown");
        }
        driver.findElement(By.xpath("//*[@id=\"normal_login_username\"]")).clear();
        Thread.sleep(3000);
    }

    @Then("I enter invalid username and password")
    public void i_enter_invalid_username_and_password() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver,30);
        WebElement email = driver.findElement(By.xpath("//*[@id=\"normal_login_username\"]"));
        WebElement password = driver.findElement(By.xpath("//*[@id=\"normal_login_password\"]"));
        WebElement login = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div[2]/div[1]/div/div/form/div[5]/div/div/span/div/div[1]/button"));

        email.sendKeys("santosh.bohora@infotmt.com");
        Thread.sleep(5000);
        password.sendKeys("Password");
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
        Thread.sleep(5000);
        login.click();

        if (driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div[2]/div[1]/div/div[1]")).isDisplayed()){
            System.out.println("Error Message is shown so test passed");
        }else {
            System.out.println("Test failed");
        }
        Thread.sleep(5000);

        password.sendKeys(Keys.CONTROL,"a");
        password.sendKeys(Keys.BACK_SPACE);
        Thread.sleep(5000);
        if (driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div[2]/div[1]/div/div/form/div[4]/div/div/div")).isDisplayed()){
            System.out.println("empty password message is shown");
        }else {
            System.out.println("test fail");
        }
        Thread.sleep(3000);
    }

    @Then("I enter valid username and password")
    public void iEnterValidUsernameAndPassword()throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver,30);
        WebElement email = driver.findElement(By.xpath("//*[@id=\"normal_login_username\"]"));
        WebElement password = driver.findElement(By.xpath("//*[@id=\"normal_login_password\"]"));
        WebElement eye_element = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div[2]/div[1]/div/div/form/div[4]/div/div/span/span/span/i"));

        email.clear();
        Thread.sleep(3000);
        password.clear();
        Thread.sleep(3000);
        email.sendKeys("santosh.bohora@infotmt.com");
        Thread.sleep(3000);
        password.sendKeys("Infotmt2019");
        Thread.sleep(3000);
        eye_element.click();
        Thread.sleep(3000);

        System.out.println("Correct email and password is entered.");
        System.out.println("Eye icon works properly");
    }

    @And("I click the Login button.")
    public void iClickTheLoginButton() {
       WebElement login = driver.findElement(By.xpath("//*[@id=\"app\"]/div/div/div[2]/div[1]/div/div/form/div[5]/div/div/span/div/div[1]/button"));
       login.click();
       driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
    }


    @Then("I check the dashboard element.")
    public void i_check_the_dashboard_element() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver,30);
       if (driver.findElement(By.xpath("/html/body/div[1]/div/section/section/section/main/div/div/div[3]/div[1]/h3")).isDisplayed()){
           System.out.println("Currently on Dashboard");
       }else{
           System.out.println("Dashboard is not loaded");
           driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
       }
        Thread.sleep(5000);
    }
    @Then("I click on Job Menu.")
    public void i_click_on_Job_Menu() {
        WebDriverWait wait = new WebDriverWait(driver,30);
        driver.findElement(By.xpath("//*[@id=\"app\"]/div/section/section/aside/div[1]/ul/li[2]")).click();
        System.out.println("Loading Job Menu");
    }

    @Then("I check if the Job page is opened.")
    public void i_check_if_the_Job_page_is_opened() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver,30);
        if (driver.findElement(By.xpath("//*[@id=\"app\"]/div/section/section/section/main/div/div/div[1]/div[1]/p")).isDisplayed()){
            System.out.println("Job portal is opened");
        }else{
            System.out.println("Job portal is not loading");
        }
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
        Thread.sleep(5000);
    }

    @Then("I open the Candidate portal.")
    public void i_open_the_Candidate_portal() {
        WebDriverWait wait = new WebDriverWait(driver,30);
        driver.findElement(By.xpath("//*[@id=\"app\"]/div/section/section/aside/div[1]/ul/li[3]")).click();
        System.out.println("Loading Candidate Portal");
    }

    @Then("I verify the Candidate page.")
    public void i_verify_the_Candidate_page() throws InterruptedException {
        WebDriverWait wait = new WebDriverWait(driver,30);
        if (driver.findElement(By.xpath("//*[@id=\"app\"]/div/section/section/section/main/div/div/div[1]/div[1]/p")).isDisplayed()){
            System.out.println("We are currently in candidate portal");
        }else {
            System.out.println("Candidate portal cannot be opened.");
            driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
        }
        Thread.sleep(5000);
    }

    @Then("I log out from Nobilis.")
    public void i_log_out_from_Nobilis() {
        WebDriverWait wait = new WebDriverWait(driver,30);
        driver.findElement(By.xpath("//*[@id=\"app\"]/div/section/div/div[3]/div/span[3]/span[2]")).click();
        driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);

        driver.findElement(By.xpath("/html/body/div[2]/div/div/ul/li")).click();
        driver.manage().timeouts().implicitlyWait(5,TimeUnit.SECONDS);

        System.out.println("Successfully Logout from NobilisAi");

    }



}
